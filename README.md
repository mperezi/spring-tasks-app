[![CircleCI](https://circleci.com/gh/mperezi/spring-tasks-app.svg?style=svg)](https://circleci.com/gh/mperezi/spring-tasks-app)

# Spring Tasks App

Demo RESTful API Server developed with Spring Boot that allows the creation, read, update and deletion of tasks. In addition, tasks can also be flagged as finished.

## Usage

Clone the repo into your local machine:

```bash
$ git clone https://github.com/mperezi/spring-tasks-app.git
```

Build and run the server via gradle:

```bash
$ cd spring-tasks-app
$ ./gradlew bootRun
```

As most Spring Boot web applications, the server will be listening for requests at http://localhost:8080.

## API Documentation

Documentation of the different HTTP methods along with the paths exposed by the server is automatically generated via Swagger during the build process. 

Swagger UI is also enabled so you can get a nice render of this documentation by reaching http://localhost:8080/swagger-ui.html. 

## Testing

While Swagger UI is quite handy for documentation purposes it also allows for some basic testing of a REST API without the need of additional tools like curl or Postman.

Additionally, as an automated way of validating the proper behaviour of the different methods, a suite of **integration tests** is shipped with the project. You can validate these tests by running:

```bash
$ ./gradlew test
```

A test summary can be later found in `build/reports/tests/test/index.html`.

## Try it live

A demo server is deployed at http://tasks.mperezi.com:8081/

API Documentation is available at http://tasks.mperezi.com:8080/swagger-ui.html