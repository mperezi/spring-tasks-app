#!/usr/bin/env bash

ssh -o StrictHostKeyChecking=no root@mperezi.com << EOF
	PS4="$ "
	set -x
	cd docker/spring-tasks-app
	git pull
	./gradlew dockerBuildImage dockerRun
EOF
